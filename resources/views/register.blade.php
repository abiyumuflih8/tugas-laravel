@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')

<h1>Buat Account Baru</h1>
<form action="/welcome" method="post">
    @csrf
<h3>Sign Up Form</h3>
<label>First name :</label><br><br>
<input type="text" name="first"><br><br>
<label>Last name</label><br><br>
<input type="text" name="last"><br><br>
<label>Gender</label><br><br>
<input type="radio" name="Gender">Male</br>
<input type="radio" name="Gender">Female</br><br>
<label>Nationality</label>
<select>
    <option>Indonesia</option>
    <option>Amerika</option>
    <option>Inggris</option>
</select><br><br>
<label>Languange Spoken</label><br><br>
<input type="checkbox" name="lang">Bahasa Indonesia</br>
<input type="checkbox" name="lang">English</br>
<input type="checkbox" name="lang">Other</br><br>
<label>Bio</label><br><br>
<textarea cols="30" rows="10"></textarea>
<a href="welcome.html"><br>
<button type="submit">Sign Up</button>
</a>
</form>
@endsection